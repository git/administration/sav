use lib './lib';
use Drogo::Server::PSGI;
use Sav::App;

my $app = sub {
    my $env = shift;

    return sub {
        my $respond = shift;

        # create new server object
        my $server = Drogo::Server::PSGI->new( env => $env, respond => $respond );

        Sav::App->handler( server  => $server );
    }
};

