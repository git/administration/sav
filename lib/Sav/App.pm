package Sav::App;
use strict;

use Drogo::Dispatch( import_drogo_methods => 1 );

sub handler_nginx
{
    my $r = shift;
    require Drogo::Server::Nginx;

    my $server_obj = Drogo::Server::Nginx->initialize($r);

    return __PACKAGE__->handler( server => $server_obj );
}

sub init
{
    my $self = shift;

    $self->{foo} = 'bar';
}

sub bad_dispatch
{
    my $self = shift;
    
    $self->header('text/html'); # default
    $self->status(404);

    $self->print('bad dispatch!');
}

sub error
{
    my $self = shift;
    
    $self->header('text/html'); # default
    $self->status(500);

    $self->print('oh gosh');
}

sub primary :Index
{
    my $self = shift;

    $self->header('text/html'); # default
    $self->status(200); # defaults to 200 anyways

    $self->print('Welcome!');
    $self->print(q[Go here: <a href="/moo">Mooville</a>]);
}

sub moo :Action
{
    my $self = shift;
    $self->print("Moo!");
    $self->print(q[Go here: <a href="/taco/forest">Taco Forest!</a>]);
}

# referenced by /zoo/whatever
sub zoo :ActionMatch
{
    my $self = shift;
    my @post_args = $self->post_args;

    $self->print('Howdy: ' . $post_args[0]);
}

sub stream_this :Action
{
    my $self = shift;

    # stop dispatcher
    $self->dispatching(0);

    $self->server->header_out('ETag' => 'fakeetag');
    $self->server->header_out('Cache-Control' => 'public, max-age=31536000');
    $self->server->send_http_header('text/html');
    $self->server->print('This was directly streamed');
}

sub cleanup
{
    my $self = shift;

    warn sprintf(
        "[%s]	%s	%s\n",
        scalar localtime,
        $self->remote_addr,
        $self->uri,
        );
}

1;

